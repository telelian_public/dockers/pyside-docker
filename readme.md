# pyside docker

## build
```bash
docker build -t registry.gitlab.com/telelian_public/dockers/pyside-docker .
docker push registry.gitlab.com/telelian_public/dockers/pyside-docker
```

## run
```bash
export DISPLAY=:0
xhost +local:docker

docker run -it --privileged --rm \
-v /tmp/.X11-unix:/tmp/.X11-unix \
-e DISPLAY=unix:0 \
-v $HOME/work:/root/work \
registry.gitlab.com/telelian_public/pyside-docker /bin/bash
```