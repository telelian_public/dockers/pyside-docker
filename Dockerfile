# build
FROM registry.gitlab.com/telelian/dock/jetson-nano-ubuntu-python-gstreamer:1.14.5-3.9.7-bionic-build as build

# Make sure we use the virtualenv:
RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

RUN apt-get update -qq \
    && apt-get install --no-install-recommends -y -qq \
    build-essential libgl1-mesa-dev perl git \
    '^libxcb.*-dev' libx11-xcb-dev libglu1-mesa-dev libxrender-dev libxi-dev libxkbcommon-dev libxkbcommon-x11-dev \
    flex bison gperf libicu-dev libxslt-dev ruby \
    libasound2-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-good1.0-dev libgstreamer-plugins-bad1.0-dev

ENV QT_VER=v5.15.8-lts-lgpl
WORKDIR /opt
RUN git clone https://code.qt.io/qt/qt5.git \
    && cd qt5 \
    && git checkout $QT_VER \
    && git submodule update --init --recursive \
    && mkdir qt5-build \
    && cd qt5-build 

WORKDIR /opt/qt5/qt5-build
RUN ../configure -prefix /opt/qt515 -release -opensource -confirm-license \
        -nomake examples -nomake tests \
        -skip qttools \
        -skip qt3d \
        -skip qtquick3d \
        -skip qtwebengine \
        -skip qtdoc \
        -skip qtdocgallery
# /opt/qt5/qtbase/configure -top-level -developer-build -opensource -nomake examples -nomake tests -confirm-license
# <srcbase> = /opt/qt5/qtbase
# <bldbase> = /opt/qt5/qtbase
# <outbase> = /opt/qt5/qtbase
RUN make -j$(nproc) && make install

COPY requirements.txt ./
ENV PATH=$PATH:/opt/qt515/bin
ENV MAKEFLAGS='-j$(nproc)'
RUN /opt/venv/bin/python -m pip install --upgrade pip wheel
RUN pip install --no-cache-dir -r requirements.txt
# python -m pip install --index-url=http://download.qt.io/snapshots/ci/pyside/5.11/latest pyside2 --trusted-host download.qt.io
# RUN python --version \ 
#     && pip install --no-cache-dir PySide2 -v
RUN pip install --no-cache-dir PyQt5 -v
# product
FROM registry.gitlab.com/telelian/dock/jetson-nano-ubuntu-python-l4t-gstreamer:1.14.5-32.7.1-3.9.7-bionic-run

# without l4t; need to volumes(/dev, /etc, /usr)
# FROM registry.gitlab.com/telelian/dock/jetson-nano-ubuntu-python-gstreamer:1.14.5-3.9.7-bionic-run

ENV LC_ALL C
ENV DEBIAN_FRONTEND noninteractive

RUN sed -i 's/# deb/deb/g' /etc/apt/sources.list

RUN apt-get update -qq \
    && apt-get install --no-install-recommends -y -qq \
    qt5-default pyqt5-dev pyqt5-dev-tools python3-pyqt5 \
    build-essential libgl1-mesa-dev \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh /
RUN chmod +x /wait-for-it.sh

ENV PYTHONUNBUFFERED=1

COPY --from=build /opt/venv /opt/venv
COPY . /opt/venv/
COPY --from=build /opt/qt515 /opt/qt515
WORKDIR /opt/venv

# without l4t; need to volumes(/dev, /etc, /usr)
# RUN ln -sf /usr/bin/python3.9 /opt/venv/bin/python

ENV PATH=/opt/venv/bin:/opt/venv:$PATH

# CMD ["python", "-m", "vdc"]
